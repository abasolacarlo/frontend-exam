# Build steps
1. Clone repository
```bash
git clone git@gitlab.com:abasolacarlo/frontend-exam.git
```
2. Go to directory
```bash
cd frontend-exam
```
3. Install dependencies
```bash
npm install
```
4. Build
```
npm run build
```

# Serve Build
```bash
npm run serve
```

# Run dev server
```
npm run dev
```